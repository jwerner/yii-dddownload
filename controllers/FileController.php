<?php
/**
 * FileController class file
 *
 * @author Joachim Werner <joachim.werner@diggin-data.de>
 * @package download.controllers
 */

class FileController extends Controller
{
    // {{{ *** Members ***
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout='//layouts/column2';

    /**
     * @var CActiveRecord the currently loaded data model instance.
     */
    private $_model;
    // }}} End Members
    // {{{ *** Methods ***
    // {{{ filters
    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    } // }}} 
    // {{{ accessRules
    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow',  // allow all users to perform 'index' and 'view' actions
                'actions'=>array('index','view','preview','download'),
                'users'=>array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions'=>array('create','update' /* ,'admin','delete' */),
                'users'=>array('@'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions'=>array('admin','delete','resetDownloadCounter'),
                'users'=>array('admin'),
            ),
            array('deny',  // deny all users
                'users'=>array('*'),
            ),
        );
    } // }}} 
    // {{{ actionView
    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        $this->render('view',array(
            'model'=>$this->loadModel($id),
        ));
    } // }}} 
    // {{{ actionPreview
    public function actionPreview($image=null, $x=0, $y=0, $src_x=0, $src_y=0, $src_w=0, $src_h=0, $resize=0, $aspectratio=1)
    {
        $image = str_replace('|',DIRECTORY_SEPARATOR, $image);
        $argKeys = array('image', 'x', 'y', 'src_x', 'src_y', 'src_w', 'src_h', 'resize', 'aspectratio');
        error_reporting(E_ALL ^ E_NOTICE);
        $types = array (1 => "gif", "jpeg", "png", "swf", "psd", "wbmp");

        // Set file name
        if(!isset($image)) {
            die('Es wurde kein Bild angegeben!');
        } else {
             if(!file_exists($image))
                 die('Die angegebene Datei konnte nicht auf dem Server gefunden werden!');
        }

        // Do we have a folder?
        if(is_dir($image)) {
            header('Content-Type: image/png');
            readfile('images/filetypeicons/folder.png');
            die;
        }

        // Do we have a non-image file?
        $mimeType = mime_content_type($image);
        if(strpos($mimeType,'image/')===false) {
            // No image
            if(preg_match("/\.([^\.]+)$/",$image,$matches)){
                $extension = $matches[1];
                $iconPath = dirname(__FILE__).'/../../../../'.str_replace(Yii::app()->baseUrl.'/', '',$this->module->assetsUrl).'/filetypeicons';
                $extensionIcon = $iconPath.'/'.$this->getFileIconByExtension($extension);
                if(!file_exists($extensionIcon))
                    $extensionIcon=$iconPath.'/fileicon_bg.png';
                header('Content-Type: image/png');
                readfile($extensionIcon);
                die;
            }
        }

        if(
        (is_null($x) && is_null($y))  
        ) {
           die('Fehlende(r) oder ungültige(r) Größenparameter!');
        }

        $cacheSubDir = '.thumbs';
        $fileDir = dirname($image);
        $cacheDir = $fileDir.'/'.$cacheSubDir;
        // Create thumbs dir?
        if(!is_dir ($cacheDir))
            @mkdir($cacheDir, 0777);

        $imagedata = getimagesize($image);

        if(!isset($imagedata[2]) || $imagedata[2] == 4 || $imagedata[2] == 5)
            die('Bei der angegebenen Datei handelt es sich nicht um ein Bild!');

        if($x==0)
            $x = floor ($y * $imagedata[0] / $imagedata[1]);

        if($y==0)
            $y = floor ($x * $imagedata[1] / $imagedata[0]);

        if ($aspectratio) {
             if ($imagedata[0] > $imagedata[1]) { // Breite > Höhe
                  $y = floor ($x * $imagedata[1] / $imagedata[0]); // Neue Breite * Verh. H/B
             } else if ($imagedata[1] > $imagedata[0]) {
                  $x = floor ($y * $imagedata[0] / $imagedata[1]); // Neue Höhe * Verh. B/H
             }
        }
        // src_w
        if($src_w==0)
            $src_w = $imagedata[0];
        // src_h
        if($src_h==0)
            $src_h = $imagedata[1];

        $dst_x = 0;
        $dst_y = 0;

        $myArgs = func_get_args();
        $tmp = array();
        for($i=1; $i<count($myArgs); $i++) {
            $tmp[] = $argKeys[$i].'='.$myArgs[$i];
        }
        $thumbfile = md5(join('&',$tmp)).'_'.basename($image);

        if (file_exists ($cacheDir.'/'.$thumbfile)) {
             $thumbdata = getimagesize ($cacheDir.'/'.$thumbfile);
             $thumbdata[0] == $x && $thumbdata[1] == $y
                  ? $iscached = true
                  : $iscached = false;
        } else {
             $iscached = false;
        }

        if (!$iscached) {
             ($imagedata[0] > $x || $imagedata[1] > $y) ||
             (($imagedata[0] < $x || $imagedata[1] < $y) && $resize)
                  ? $makethumb = true
                  : $makethumb = false;
        } else {
             $makethumb = false;
        }

        Header( "Content-Type: image/".$types[$imagedata[2]] );

        if ($makethumb) {
             $image = call_user_func("imagecreatefrom".$types[$imagedata[2]], $image);
             $thumb = imagecreatetruecolor ($x, $y);
             // imagecopyresized ( resource $dst_image , resource $src_image , int $dst_x , int $dst_y , int $src_x , int $src_y , int $dst_w , int $dst_h , int $src_w , int $src_h ) 
             imagecopyresized (
                 $thumb,    // destination image
                 $image,    // source image 
                 $dst_x,    // dest. x
                 $dst_y,    // dest. y
                 $src_x,    // source x
                 $src_y,    // source y
                 $x,        // dest. width
                 $y,        // dest. height
                 $src_w, 
                 $src_h
             );
             call_user_func("image".$types[$imagedata[2]], $thumb, $cacheDir.'/'.$thumbfile);
             imagedestroy ($image);
             imagedestroy ($thumb);
             $image = $cacheDir.'/'.$thumbfile;
        } else {
             $iscached
                  ? $image = $cacheDir.'/'.$thumbfile
                  : $image = $image;
        }
        Header( "Content-Length: ".filesize($image) );
        $image = fopen ($image, "rb");
        fpassthru ($image);
        fclose ($image);
        die;
    } // }}}
    // {{{ actionDownload
    public function actionDownload($id)
    {
        $model = $this->loadModel($id);
        header("Content-Type: ".$model->mimeType);
        header("Content-Disposition: attachment; filename=\"{$model->fileName}\"");
        readfile($model->pathToFile.DIRECTORY_SEPARATOR.md5($model->fileName));
        $model->incrementDownloadCounter();
        die;
    } // }}} 
    // {{{ actionResetDownloadCounter
    public function actionResetDownloadCounter($id)
    {
        $model = $this->loadModel($id);
        $model->resetDownloadCounter();
        $this->redirect(array('view', 'id'=>$id));
    } // }}} 
    // {{{ actionCreate
    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {
        $model          = new DownloadFile;
        $model->version = 1;

        if(isset($_GET['DownloadFile']))
            $model->attributes=$_GET['DownloadFile'];

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if(isset($_POST['DownloadFile']))
        {
            $model->attributes  = $_POST['DownloadFile'];
            $model->mimeType    = $_FILES['DownloadFile']['type']['pathToFile'];
            $uploadedFile       = CUploadedFile::getInstance($model,'pathToFile');
            $model->pathToFile  = $this->module->baseDir;
            $model->fileName    = $uploadedFile->name;

            if($model->save()) {
                if(!empty($uploadedFile))  // check if uploaded file is set or not
                {
                    $uploadedFile->saveAs($model->pathToFile.DIRECTORY_SEPARATOR.md5($model->fileName));  // file will uplode to rootDirectory/files/
                }
                Yii::app()->user->setFlash('success',Yii::t('DownloadModule.main','<strong>Creating New Item</strong><br />The item with ID #{id} has been created.',array('{id}'=>$model->id)));
                $this->redirect(array('view','id'=>$model->id));
            } else {
                Yii::app()->user->setFlash('error',Yii::t('DownloadModule.main','<strong>Error Creating New Item</strong><br />Couldn\'t create a new item.'));
            }
        }

        $this->render('create',array(
            'model'=>$model,
        ));
    } // }}} 
    // {{{ actionUpdate
    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        $model=$this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if(isset($_POST['DownloadFile']))
        {
            $model->attributes=$_POST['DownloadFile'];
            $model->mimeType    = $_FILES['DownloadFile']['type']['pathToFile'];
            $uploadedFile=CUploadedFile::getInstance($model,'pathToFile');
            if(!empty($uploadedFile))  // check if uploaded file is set or not
            {
                $model->pathToFile  = $this->module->baseDir;
                $model->fileName    = $uploadedFile->name;
                $uploadedFile->saveAs($model->pathToFile.DIRECTORY_SEPARATOR.md5($model->fileName));
            }
            if($model->save()) {
                
                Yii::app()->user->setFlash('success',Yii::t('DownloadModule.main','<strong>Updating Item</strong><br />The item with ID #{id} has been updated.',array('{id}'=>$model->id)));
                $this->redirect(array('view','id'=>$model->id));
            } else {
                Yii::app()->user->setFlash('error',Yii::t('DownloadModule.main','<strong>Error Updating Item</strong><br />Couldn\'t update item with ID #{id}.',array('{id}'=>$model->id)));
            }
        }

        $this->render('update',array(
            'model'=>$model,
        ));
    } // }}} 
    // {{{ actionDelete
    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        if(Yii::app()->request->isPostRequest)
        {
            // we only allow deletion via POST request
            $result = $this->loadModel($id)->delete();

            // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
            if(!isset($_GET['ajax'])) {
                if($result==true) {
                    Yii::app()->user->setFlash('success',Yii::t('DownloadModule.main','<strong>Deleting Item</strong><br />The item with ID #{id} has been deleted.',array('{id}'=>$model->id)));
                    $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
                } else {
                    Yii::app()->user->setFlash('error',Yii::t('DownloadModule.main','<strong>Error on Deleting Item</strong><br />The item with ID #{id} couldn\'t be deleted.',array('{id}'=>$model->id)));
                }
            }
        }
        else
            throw new CHttpException(400,Yii::t('DownloadModule.main','Invalid request. Please do not repeat this request again.'));
    } // }}} 
    // {{{ actionIndex
    /**
     * Lists all models.
     */
    public function actionIndex()
    {
        $model=new DownloadFile('search');
        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['DownloadFile']))
            $model->attributes=$_GET['DownloadFile'];
        $dataProvider=$model->search(); //new CActiveDataProvider('DownloadFile');
        $this->render('index',array(
            'dataProvider'=>$dataProvider,
        ));
    } // }}} 
    // {{{ actionAdmin
    /**
     * Manages all models.
     */
    public function actionAdmin()
    {
        $model=new DownloadFile('search');
        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['DownloadFile']))
            $model->attributes=$_GET['DownloadFile'];

        $this->render('admin',array(
            'model'=>$model,
        ));
    } // }}} 
    // {{{ loadModel
    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id)
    {
        $model=DownloadFile::model()->findByPk($id);
        if($model===null)
            throw new CHttpException(404,Yii::t('DownloadModule.main','The requested page does not exist.'));
        return $model;
    } // }}} 
    // {{{ performAjaxValidation
    /**
     * Performs the AJAX validation.
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if(isset($_POST['ajax']) && $_POST['ajax']==='download-file-form')
        {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    } // }}} 
    // {{{ getFileIconByExtension
    /**
     * Returns an icon filename by file extension
     *
     * @param string $extension Extension (e.g. 'xls', 'css', 'txt' ...)
     * @return string Filetype icon name (e.g. 'excel.png')
     */
    private function getFileIconByExtension($extension)
    {
        $icons = array(
            'zip'=>'compressed',
            'css'=>'css',
            'ini'=>'developer',
            'php'=>'developer',
            'vb'=>'developer',
            'bash'=>'developer',
            'sh'=>'developer',
            'xlsb'=>'excel',
            'xlsx'=>'excel',
            'xls'=>'excel',
            //'fileicon_bg',
            //'fireworks',
            'flv'=>'flash',
            'swf'=>'flash',
            //'folder',
            'htm'=>'html',
            'html'=>'html',
            //'illustrator',
            'jpg'=>'image',
            'png'=>'image',
            'keynote',
            'avi'=>'movie',
            'mp4'=>'movie',
            'avi'=>'movie',
            'divx'=>'movie',
            'mpg'=>'movie',
            'mpeg'=>'movie',
            'mp3'=>'music',
            'wav'=>'music',
            'csv'=>'numbers',
            // 'pages',
            'pdf'=>'pdf',
            'psd'=>'photoshop',
            'pptx'=>'powerpoint',
            'ppt'=>'powerpoint',
            'txt'=>'text',
            'md'=>'text',
            'mkd'=>'text',
            'markdown'=>'text',
            //'unknown',
            'doc'=>'word',
            'docx'=>'word',
        );
        if(array_key_exists($extension, $icons))
            return $icons[$extension].'.png';
        else
            return 'unknown.png';
    } // }}} 
    // }}} 
}
