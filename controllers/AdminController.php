<?php
/**
 * AdminController class file.
 * @author Joachim Werner <joachim.werner@diggin-data.de>
 * @copyright Copyright &copy; Joachim Werner 2013-
 * @license http://www.opensource.org/licenses/bsd-license.php New BSD License
 * @package download.controllers
 */

/**
 * Controller for managing downloads
 */
class AdminController extends Controller
{
    // {{{ *** Members ***
    public $layout = '//layouts/column2';
    // }}} 
    // {{{ *** Methods ***
    /**
     * Declares class-based actions.
     */
    public function actions()
    {
        return array(
            // page action renders "static" pages stored under 'protected/views/site/pages'
            // They can be accessed via: index.php?r=site/page&view=FileName
            'page'=>array(
                'class'=>'CViewAction',
            ),
        );
    } // }}} 
    public function actionIndex()
	{
		$this->render('index');
    }
    // }}} End Methods
}
