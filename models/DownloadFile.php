<?php
/**
 * DownloadFile class file
 *
 * @author Joachim Werner <joachim.werner@diggin-data.de>
 * @package download.models
 */

/**
 * This is the model class for table "{{dl_file}}".
 *
 * The followings are the available columns in table '{{dl_file}}':
 * @property integer $id
 * @property integer $categoryId
 * @property string $title
 * @property string $subTitle
 * @property string $pathToFile
 * @property string $description
 * @property string $version
 */
class DownloadFile extends Model
{
    // {{{ *** Methods ***
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return DownloadFile the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{dl_file}}';
	}
    // {{{ behaviors
    public function behaviors()
    {
        return array(
            'datetimeI18NBehavior' => array(
                // 'ext' is in Yii 1.0.8 version. For early versions, use 'application.extensions' instead.
                'class' => 'ext.DateTimeI18NBehavior'
            ),
            'DDTimestampBehavior' => array(
                'class'                 => 'ext.diggindata.ddtimestamp.behaviors.DDTimestampBehavior',
                'timestampTableName'    => '{{artimestamp}}',
                'userClassName'         => 'User',
                'userNameAttribute'     => 'recordName',
                'setUpdateOnCreate'     => true,
            )
        ); 
    } // }}} 
    /**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('categoryId, title', 'required'),
			array('categoryId', 'numerical', 'integerOnly'=>true),
			array('title, subTitle, mimeType', 'length', 'max'=>255),
			array('version', 'length', 'max'=>20),
            array('pathToFile, fileName, description', 'safe'),
            array('pathToFile', 'file','types'=>Yii::app()->getModule('download')->allowedFileTypes, 'on'=>'insert'), // this will allow empty field when page is update (remember here i create scenario update)
            array('pathToFile', 'file','types'=>Yii::app()->getModule('download')->allowedFileTypes, 'allowEmpty'=>true, 'on'=>'update'), // this will allow empty field when page is update (remember here i create scenario update)
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, categoryId, title, subTitle, pathToFile, description, version, downloadCounter', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
        return array(
            'category'=>array(self::BELONGS_TO, 'DownloadCategory','categoryId'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => Yii::t('DownloadModule.main','ID'),
			'categoryId' => Yii::t('DownloadModule.main','Category'),
			'title' => Yii::t('DownloadModule.main','Title'),
			'subTitle' => Yii::t('DownloadModule.main','subTitle'),
			'pathToFile' => Yii::t('DownloadModule.main','Path To File'),
			'fileName' => Yii::t('DownloadModule.main','File Name'),
			'description' => Yii::t('DownloadModule.main','Description'),
			'version' => Yii::t('DownloadModule.main','Version'),
			'downloadCounter' => Yii::t('DownloadModule.main','Download Counter'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('categoryId',$this->categoryId);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('subTitle',$this->subTitle,true);
		$criteria->compare('pathToFile',$this->pathToFile,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('version',$this->version,true);
		$criteria->compare('downloadCounter',$this->downloadCounter);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
    }
    public function getRelativePath()
    {
        $baseDir = dirname(realpath(Yii::app()->getModule('download')->baseDir));
        return str_replace($baseDir.DIRECTORY_SEPARATOR,'',$this->pathToFile);
    }
    // {{{ getRecordName
    public function getRecordName()
    {
        return basename($this->pathToFile);
    } // }}} 
    // {{{ getMimeTypes
    public function getMimeTypes()
    {
        $mimeTypes = <<< EOL
.avi>video/x-msvideo	 
.ppt>application/mspowerpoint
.class>application/octet-stream	 
.ppz>application/mspowerpoint
.css>text/css	 
.ps>application/postscript
.doc>application/msword	 
.qt>video/quicktime
.eps>application/postscript	 
.ra>audio/x-realaudio
.exe>application/octet-stream	 
.ram>audio/x-pn-realaudio
.gif>image/gif> 
.rm>audio/x-pn-realaudio
.gtar>application/x-gtar> 
.rpm>audio/x-pn-realaudio-plugin
.gz>application/x-gzip> 
.rtf>text/rtf
.htm>text/html> 
.rtx>text/richtext
.html>text/html> 
.sgm>text/sgml
.jpe>image/jpeg> 
.sgml>text/sgml
.jpeg>image/jpeg> 
.tar>application/x-tar
.jpg>image/jpeg> 
.tcl>application/x-tcl
.js>application/x-javascript> 
.tif>image/tiff
.midi>audio/midi> 
.tiff>image/tiff
.mov>video/quicktime> 
.txt>text/plain
.movie>video/x-sgi-movie> 
.vrml>model/vrml
.mp2>audio/mpeg> 
.wav>audio/x-wav
.mp3>audio/mpeg> 
.wrl>model/vrml
.mpe>video/mpeg> 
.xbm>image/x-xbitmap
.mpeg>video/mpeg> 
.xlc>application/vnd.ms-excel
.mpg>video/mpeg> 
.xll>application/vnd.ms-excel
.mpga>audio/mpeg> 
.xlm>application/vnd.ms-excel
.pbm>image/x-portable-bitmap> 
.xls>application/vnd.ms-excel
.pdf>application/pdf> 
.xlw>application/vnd.ms-excel
.png>image/png> 
.xml>text/xml
.pps>application/mspowerpoint> 
.zip>application/zip
EOL;
        $lines = explode("\n", $mimeTypes);
        $result = array();
        foreach( $lines as $line) {
            list($extension, $mimeType) = explode('>', trim($line));
            $result[$extension] = $mimeType;
        }
        return $result;
    } // }}} 
    // {{{ getStats
    public function getStats()
    {
        // open a file
        $fp = fopen($this->pathToFile.DIRECTORY_SEPARATOR.md5($this->fileName), "r");
        // gather statistics
        $fstat = fstat($fp);
        // close the file
        fclose($fp);
        return $fstat;
    } // }}} 
    // {{{ formatBytes
    public function formatBytes($a_bytes)
    {
        if ($a_bytes < 1024) {
             return $a_bytes .' B';
        } elseif ($a_bytes < 1048576) {
             return round($a_bytes / 1024, 2) .' KiB';
        } elseif ($a_bytes < 1073741824) {
             return round($a_bytes / 1048576, 2) . ' MiB';
        } elseif ($a_bytes < 1099511627776) {
             return round($a_bytes / 1073741824, 2) . ' GiB';
        } elseif ($a_bytes < 1125899906842624) {
             return round($a_bytes / 1099511627776, 2) .' TiB';
        } elseif ($a_bytes < 1152921504606846976) {
             return round($a_bytes / 1125899906842624, 2) .' PiB';
        } elseif ($a_bytes < 1180591620717411303424) {
             return round($a_bytes / 1152921504606846976, 2) .' EiB';
        } elseif ($a_bytes < 1208925819614629174706176) {
             return round($a_bytes / 1180591620717411303424, 2) .' ZiB';
        } else {
             return round($a_bytes / 1208925819614629174706176, 2) .' YiB';
        }
    } // }}} 
    // {{{ incrementDownloadCounter
    /**
     * Increments the counter of downloads
     *
     * Also sets the timestamp on lastDownloadedTs
     */
    public function incrementDownloadCounter()
    {
        $sql = "UPDATE ".$this->tableName()." SET downloadCounter=downloadCounter+1, lastDownloadedTs=:mktime WHERE id=:id";
        return $this->dbConnection->createCommand($sql)->execute(array(':id'=>$this->id, ':mktime'=>time()));
    } // }}} 
    /**
     * Resets the counter of downloads
     *
     * Also sets the timestamp on lastDownloadedTs to NULL
     */
    public function resetDownloadCounter()
    {
        $sql = "UPDATE ".$this->tableName()." SET downloadCounter=0, lastDownloadedTs=NULL WHERE id=:id";
        return $this->dbConnection->createCommand($sql)->execute(array(':id'=>$this->id));
    } // }}} 
    // }}} End Methods   
}
