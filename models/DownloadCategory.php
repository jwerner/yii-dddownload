<?php

/**
 * This is the model class for table "{{dl_category}}".
 *
 * The followings are the available columns in table '{{dl_category}}':
 * @property integer $id
 * @property integer $parentId
 * @property string $title
 * @property integer $description
 * @property string $authItems
 * @property integer $status
 */
class DownloadCategory extends Model
{
    // {{{ *** Members ***
    /**
     * @var string
     */
    public $nameFields = "title";
    /**
     * @var string
     */
    public $nameFormat = "{1}";

    public $filesCount;
    
    const STATUS_DRAFT=1;
    const STATUS_PUBLISHED=2;
    const STATUS_ARCHIVED=3;
    // }}} End Members
    // {{{ *** Methods ***
    // {{{ model
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return DownloadCategory the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	} // }}} 
    // {{{ tableName
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{dl_category}}';
	} // }}} 
    // {{{ behaviors
    public function behaviors()
    {
        return array(
            'datetimeI18NBehavior' => array(
                // 'ext' is in Yii 1.0.8 version. For early versions, use 'application.extensions' instead.
                'class' => 'ext.DateTimeI18NBehavior'
            ),
            'DDTimestampBehavior' => array(
                'class'                 => 'ext.diggindata.ddtimestamp.behaviors.DDTimestampBehavior',
                'timestampTableName'    => '{{artimestamp}}',
                'userClassName'         => 'User',
                'userNameAttribute'     => 'recordName',
                'setUpdateOnCreate'     => true,
            )
        ); 
    } // }}} 
    // {{{ rules
	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('parentId, title, status', 'required'),
			array('parentId, status', 'numerical', 'integerOnly'=>true),
            array('title', 'length', 'max'=>255),
            array('description, authItems', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, parentId, title, description, authItems, status, filesCount', 'safe', 'on'=>'search'),
		);
	} // }}} 
    // {{{ relations
	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
        return array(
            'parent'=>array(self::BELONGS_TO,'DownloadCategory', 'parentId'),
            'children'=>array(self::HAS_MANY,'DownloadCategory', 'parentId', 'order'=>'title'),
            'files'=>array(self::HAS_MANY,'DownloadFile','categoryId'),
		);
	} // }}} 
    // {{{ attributeLabels
	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => Yii::t('DownloadModule.main','ID'),
			'parentId' => Yii::t('DownloadModule.main','Parent Category'),
			'title' => Yii::t('DownloadModule.main','Title'),
			'description' => Yii::t('DownloadModule.main','Description'),
			'authItems' => Yii::t('DownloadModule.main','Auth Items'),
            'status' => Yii::t('DownloadModule.main','Status'),
            'filesCount' => Yii::t('DownloadModule.main','Files'),
		);
	} // }}} 
    // {{{ search
	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

        $criteria=new CDbCriteria;
        $criteria->with = array('files');

        // sub query to retrieve the count of posts
        $files_table = DownloadFile::model()->tableName();
        $files_count_sql = "(select count(*) from $files_table where $files_table.categoryId = t.id)";

        // select
        $criteria->select = array(
            '*',
            $files_count_sql . " AS filesCount",
        );
        
		$criteria->compare('id',$this->id);
		$criteria->compare('parentId',$this->parentId);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('description',$this->description);
		$criteria->compare('authItems',$this->authItems,true);
		$criteria->compare('status',$this->status);

		return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'sort' => array(
                'defaultOrder' => 't.title',
                'attributes' => array(
                    // order by
                    'filesCount' => array(
                        'asc' => 'filesCount ASC',
                        'desc' => 'filesCount DESC',
                    ),
                    '*',
                ),
            ),
		));
	} // }}} 
    // {{{ getStatusOptions
    /**
     * Returns an array of status descriptions, indexed by status codes
     *
     * @return mixed
     */
    public function getStatusOptions()
    {
        return array(
            self::STATUS_DRAFT      => Yii::t('BlogModule.main','Draft'),
            self::STATUS_PUBLISHED  => Yii::t('BlogModule.main','Published'),
            self::STATUS_ARCHIVED   => Yii::t('BlogModule.main','Archived'),
        );
    } // }}} 
    // {{{ getParentOptions
    public function getParentOptions()
    {
        $result = array(
            0=>Yii::t('DownloadModule.main','(Root)')
        );
        foreach($this->findAll(array('order'=>'title')) as $model)
            $result[$model->id] = $model->title;

        return $result;
    } // }}} 
    // {{{ getParentLink
    public function getParentLink($target='category')
    {
        if($this->parentId==0)
            return Yii::t('DownloadModule.main','(Root)');
        if(!is_null($this->parent)) {
            if($target=='category')
                return CHtml::link($this->parent->recordName, array('category/view', 'id'=>$this->parentId));
            elseif($target=='files')
                return '<i class="icon-arrow-up"></i> '.CHtml::link($this->parent->recordName, array('file/index', 'DownloadFile[categoryId]'=>$this->parentId));
        }
        else
            return '?';

    } // }}} 
    // {{{ getChildrenLinks
    public function getChildrenLinks($target='category')
    {
        $result = array();
        foreach($this->children as $child) {
            if($target=='category')
                $result[] = CHtml::link($child->recordName, array('category/view', 'id'=>$child->id));
            elseif( $target=='files' )
                $result[] = '<i class="icon-arrow-right"></i> '.CHtml::link($child->recordName, array('file/index', 'DownloadFile[categoryId]'=>$child->id));
        }
        if(count($result)==0)
            $result = array(Yii::t('DownloadModule.main','(none)'));
        return $result;
    } // }}} 
    // }}} End Methods
}
