-- Adminer 3.6.3 MySQL dump

SET NAMES utf8;
SET foreign_key_checks = 0;
SET time_zone = 'Europe/Paris';
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

CREATE TABLE `tbl_dl_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  `parentId` int(11) NOT NULL COMMENT 'Parent Category FK',
  `title` varchar(255) NOT NULL,
  `description` text,
  `status` tinyint(1) NOT NULL,
  `authItems` text,
  PRIMARY KEY (`id`),
  KEY `parentId` (`parentId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Download Categories';


CREATE TABLE `tbl_dl_file` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  `categoryId` int(11) NOT NULL COMMENT 'Category FK',
  `title` varchar(255) NOT NULL,
  `subTitle` varchar(255) NOT NULL,
  `pathToFile` text,
  `fileName` text NOT NULL,
  `mimeType` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `version` varchar(20) NOT NULL,
  `downloadCounter` int(11) NOT NULL DEFAULT '0' COMMENT 'Counter for downloads',
  `lastDownloadedTs` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `categoryId` (`categoryId`),
  CONSTRAINT `tbl_dl_file_ibfk_1` FOREIGN KEY (`categoryId`) REFERENCES `tbl_dl_category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- 2013-02-08 15:54:15
