<?php
/**
 * DownloadModule class file.
 * @author Joachim Werner <joachim.werner@diggin-data.de>
 * @copyright Copyright &copy; Joachim Werner 2013-
 * @license http://www.opensource.org/licenses/bsd-license.php New BSD License
 * @package download
 */


class DownloadModule extends CWebModule
{
    // {{{ *** Members ***
    public $logTag = 'application.modules.download';

    public $baseDir;
    
    public $tableCssClass;

    public $allowedFileTypes = 'jpg, png, sql';

    public $defaultController='category';

    // getAssetsUrl()
    //    return the URL for this module's assets, performing the publish operation
    //    the first time, and caching the result for subsequent use.
    private $_assetsUrl;

    // // }}}
	public function init()
	{
		// this method is called when the module is being created
		// you may place code here to customize the module or the application

		// import the module-level models and components
		$this->setImport(array(
			'download.models.*',
			'download.components.*',
		));
        
        //register translation messages from module dbadmin
        //so no need do add to config/main.php
        /*
        Yii::app()->setComponents( array(
            'messages' => array(
            'class'=>'CPhpMessageSource',
            'basePath'=>'protected/modules/media/messages',
        )));
         */

        // Check if base dir exists?
        if(!is_dir($this->baseDir)) {
            // No, create it
            @mkdir($this->baseDir);
            if(is_dir($this->baseDir)) {
                Yii::app()->user->setFlash('success', array(
                    Yii::t('DownloadModule.main','Missing Base Directory'),
                    Yii::t('DownloadModule.main',"The base directory has been created:<ul><li>{baseDir}</li></ul>", array('{baseDir}'=>realpath($this->baseDir)))
                ));
            }
        } else {
            // Sanitize path
            $this->baseDir = realpath($this->baseDir);
        }
	}
    public function getAssetsUrl()
    {
        if ($this->_assetsUrl === null)
            $this->_assetsUrl = Yii::app()->getAssetManager()->publish(
                Yii::getPathOfAlias('media.assets') );
        return $this->_assetsUrl;
    }
	public function beforeControllerAction($controller, $action)
	{
		if(parent::beforeControllerAction($controller, $action))
		{
			// this method is called before any module controller action is performed
			// you may place customized code here
			return true;
		}
		else
			return false;
	}
}
