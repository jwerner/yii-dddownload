<?php
/**
 * Message translations.
 *
 * This file is automatically generated by 'yiic message' command.
 * It contains the localizable messages extracted from source code.
 * You may modify this file by translating the extracted messages.
 *
 * Each array element represents the translation (value) of a message (key).
 * If the value is empty, the message is considered as not translated.
 * Messages that no longer need translation will have their translations
 * enclosed between a pair of '@@' marks.
 *
 * Message string can be used with plural forms format. Check i18n section
 * of the guide for details.
 *
 * NOTE, this file must be saved in UTF-8 encoding.
 */
return array (
  '(Root)' => '',
  '<strong>Creating New Item</strong><br />The item with ID #{id} has been created.' => '',
  '<strong>Deleting Item</strong><br />The item with ID #{id} has been deleted.' => '',
  '<strong>Error Creating New Item</strong><br />Couldn\'t create a new item.' => '',
  '<strong>Error Updating Item</strong><br />Couldn\'t update item with ID #{id}.' => '',
  '<strong>Error on Deleting Item</strong><br />The item with ID #{id} couldn\'t be deleted.' => '',
  '<strong>Updating Item</strong><br />The item with ID #{id} has been updated.' => '',
  'Add New File' => '',
  'Add new file to category' => '',
  'Advanced Search' => '',
  'Archived' => '',
  'Auth Items' => '',
  'Category' => '',
  'Create' => '',
  'Create New Download Category' => '',
  'Create New Download File' => '',
  'Create New DownloadCategory' => '',
  'Create New DownloadFile' => '',
  'Create a New Download Category' => '',
  'Create a New Download File' => '',
  'Delete Download Category' => '',
  'Delete Download File' => '',
  'Description' => '',
  'Download Categories' => '',
  'Download Files' => '',
  'Download {fileName}' => '',
  'Draft' => '',
  'Files' => '',
  'ID' => '',
  'Invalid request. Please do not repeat this request again.' => '',
  'List Download Categories' => '',
  'List Download Files' => '',
  'Manage' => '',
  'Manage Download Categories' => '',
  'Manage Download Files' => '',
  'Missing Base Directory' => '',
  'Parent Category' => '',
  'Path To File' => '',
  'Published' => '',
  'Reset' => '',
  'Save' => '',
  'Search' => '',
  'Status' => '',
  'The base directory has been created:<ul><li>{baseDir}</li></ul>' => '',
  'The requested page does not exist.' => '',
  'Title' => '',
  'Update' => '',
  'Update Download Category' => '',
  'Update Download Category {recordName}' => '',
  'Update Download File' => '',
  'Update Download File #{primaryKey}' => '',
  'Version' => '',
  'View Download Category' => '',
  'View Download Category {recordName}' => '',
  'View Download File' => '',
  'View Download File {recordName}' => '',
  'subTitle' => '',
);
