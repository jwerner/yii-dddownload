dddownload
=======

**DDDownload** is a module for the [Yii PHP framework](http://www.yiiframework.com) that provides a web user interface to manage downloadable files. 

Supported tasks so far:

* Browse categories
* Create, update, delete a category
* Upload a file
* Rename, copy, move, delete files
* Thumbnail preview for images
* Download for files

# Setup

Download the latest release from [Yii extensions](http://www.yiiframework.com/extension/dddownload)
or clone the Gir repository from [BitBucket](https://bitbucket.org/jwerner/yii-dddownload).

## Zip File

Unzip the module under ***protected/modules*** and rename it to ***download***

## Git Repository

~~~
$ cd path/to/your/yii/app
$ ls
assets   protected ...
$ git submodule add https://jwerner@bitbucket.org/jwerner/yii-dddownload.git modules/download
~~~

This will create a new directory _protected/modules/download_ with this extension.

## Database

To create two required tables, load the file _protected/modules/download/data/schema.mysql.sql_ into your database.

## Configuration

Add the following to your application config file _protected/main.php_:

~~~
[php]
return array(
    ...
    'modules'=>array(
        ...
        'download'=>array(
            // Base dir for download files,
            // e.g. /appdir/files/download
            'baseDir'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'files'.DIRECTORY_SEPARATOR.'download',
            // list of allowed file types for uploading:
            'allowedFileTypes'=>'jpg, png, sql',
        ),
        ...
    ...
);
~~~
***protected/config/main.php***


# Usage

* **Users Views:**  
  Add a link somewhere in your main menu like `CHtml::link('Downloads', array('/download'))`.
* **Admin Views:**  
  You can get to the admin view via `CHtml::link('Downloads Admin', array('/download/admin'))`

# To Do's

* Add required user roles (RBAC) to categories and files
* Add user ratings to files?

