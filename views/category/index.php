<?php
$this->breadcrumbs=array(
    Yii::t('DownloadModule.main','Downloads')=>array('/download'),
	Yii::t('DownloadModule.main','Download Categories'),
);

$this->menu=array(
	array('label'=>Yii::t('DownloadModule.main','Create New Download Category'),'url'=>array('create'), 'icon'=>'star'),
	array('label'=>Yii::t('DownloadModule.main','Manage Download Categories'),'url'=>array('admin'), 'icon'=>'th-list'),
);
?>

<h1><?php echo CHtml::encode(Yii::t('DownloadModule.main','Download Categories')); ?></h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
    'itemView'=>'_view',
    'template'=>'{sorter}{pager}{summary}{items}{sorter}{pager}',
    'sortableAttributes'=>array(
    'parentId',
    'title',
    'description',
    // TODO: Uncomment the following attributes to show them as sort links in list view
// 'authItems',
    // 'status',
    ),
)); ?>
