<?php
$this->breadcrumbs=array(
    Yii::t('DownloadModule.main','Downloads')=>array('/download'),
	Yii::t('DownloadModule.main','Download Categories')=>array('index'),
	$model->title,
);

$this->menu=array(
	array('label'=>Yii::t('DownloadModule.main','Update Download Category'),'url'=>array('update','id'=>$model->id), 'icon'=>'pencil'),
	array('label'=>Yii::t('DownloadModule.main','User View'),'url'=>array('file/index','DownloadFile[categoryId]'=>$model->id), 'icon'=>'zoom-in'),
	array('label'=>Yii::t('DownloadModule.main','Add New File'),'url'=>array('file/create','DownloadFile[categoryId]'=>$model->id), 'icon'=>'star'),
	array('label'=>Yii::t('DownloadModule.main','Delete Download Category'),'url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?'), 'icon'=>'remove'),
	array('label'=>Yii::t('DownloadModule.main','Create New Download Category'),'url'=>array('create'), 'icon'=>'star'),
	array('label'=>Yii::t('DownloadModule.main','Manage'),'url'=>array('admin'), 'icon'=>'th-list'),
    array('label'=>Yii::t('DownloadModule.main','Related Links'),'itemOptions'=>array('class'=>'nav-header')),
    array('label'=>Yii::t('DownloadModule.main','Download Files'),'url'=>array('file/admin'), 'icon'=>'th-list'),
);
?>

<h1><?php echo CHtml::encode(Yii::t('DownloadModule.main','View Download Category {recordName}',array('{recordName}'=>$model->recordName))); ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		array(
            'name'=>'parentId',
            'type'=>'raw',
            'value'=>$model->parentLink,
        ),
		'title',
        array(
            'name'=>'description',
            'type'=>'raw',
            'value'=>nl2br(CHtml::encode($model->description)),
        ),
		'authItems',
        array(
            'name'=>'status',
            'value'=>$model->statusOptions[$model->status],
        ),
        array(
            'label'=>'Sub Categories',
            'type'=>'raw',
            'value'=>'<ul><li>'.join('</li><li>',$model->childrenLinks).'</li></ul>',
        ),
        array(
            'label'=>Yii::t('DownloadModule.main', 'Files'),
            'value'=>CHtml::link(count($model->files),array('file/admin', 'DownloadFile[categoryId]'=>$model->id))
                .'<br />'.CHtml::link('<i class="icon-star icon-white"></i> '.Yii::t('DownloadModule.main','Add New File'),array('file/create', 'DownloadFile[categoryId]'=>$model->id), array('class'=>'btn btn-primary btn-small')),
            'type'=>'raw',
        ),      
	),
)); ?>

<?php $this->widget('ext.diggindata.ddtimestamp.widgets.DDTimestamp', array(
    'model'=>$model, 
    'title'=>Yii::t('site','Change Log'),
    'detailViewWidget'=>'bootstrap.widgets.TbDetailView'
)); ?>
