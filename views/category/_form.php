<div class="form">

<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
    'id'=>'download-category-form',
    'enableAjaxValidation'=>false,
    'type'=>'horizontal',
    'focus'=>array($model,'parentId'),
)); ?>

    <p class="help-block">Fields with <span class="required">*</span> are required.</p>

    <?php echo $form->errorSummary($model); ?>

    <?php echo $form->dropDownListRow($model,'parentId',$model->parentOptions, array('class'=>'span5')); ?>

    <?php echo $form->textFieldRow($model,'title',array('class'=>'span5','maxlength'=>255)); ?>

    <?php echo $form->textAreaRow($model,'description',array('class'=>'span5', 'rows'=>3)); ?>

    <?php echo $form->textAreaRow($model,'authItems',array('rows'=>6, 'class'=>'span5')); ?>

    <?php echo $form->radioButtonListRow($model,'status', $model->statusOptions); ?>

    <div class="form-actions">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'type'=>'primary',
            'label'=>$model->isNewRecord ? Yii::t('DownloadModule.main','Create') : Yii::t('DownloadModule.main','Save'),
            'icon'=>'ok',
        )); ?>
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'reset', 
            'icon'=>'repeat', 
            'label'=>Yii::t('DownloadModule.main','Reset'))); ?>
    </div>

<?php $this->endWidget(); ?>


</div><!-- form -->
