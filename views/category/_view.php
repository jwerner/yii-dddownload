<div class="navbar-inner" style="border: 1px solid navy; padding:15px">

    <div class="row-fluid">

    <div class="span3">

	<b><?php echo CHtml::encode($data->getAttributeLabel('title')); ?>:</b><br />
	<?php echo CHtml::link(CHtml::encode($data->title),array('file/index','DownloadFile[categoryId]'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('description')); ?>:</b><br />
	<?php echo trim($data->description)=='' ? '&ndash;' : CHtml::encode($data->description); ?>
	<br />

    </div>

    <div class="span3">

	<b><?php echo CHtml::encode($data->getAttributeLabel('parentId')); ?>:</b><br />
	<?php echo $data->getParentLink('files'); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('status')); ?>:</b><br />
	<?php echo CHtml::encode($data->statusOptions[$data->status]); ?>
	<br />

    <b><?php echo CHtml::encode(Yii::t('DownloadModule.main', 'Files')); ?>:</b><br />
    <?php echo CHtml::link(Yii::t('DownloadModule.main','{n} file|{n} files',array(count($data->files))),array('file/index', 'DownloadFile[categoryId]'=>$data->id)); ?>
    <br />

    </div>

    <div class="span3">

    <b><?php echo CHtml::encode(Yii::t('DownloadModule.main', 'Sub-Categories')); ?>:</b>
    <ul style="list-style-type:none">
        <li><?php echo join("</li>\n\t\t\t<li>", $data->getChildrenLinks('files')); ?></li>
    </ul>
    <br />
    </div>

    </div><!-- end div.row-fluid -->

</div>
