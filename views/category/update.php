<?php
$this->breadcrumbs=array(
    Yii::t('DownloadModule.main','Downloads')=>array('/download'),
	Yii::t('DownloadModule.main','Download Categories')=>array('index'),
	$model->title=>array('view','id'=>$model->id),
	Yii::t('DownloadModule.main','Update'),
);

$this->menu=array(
	array('label'=>Yii::t('DownloadModule.main', 'Create New Download Category'),'url'=>array('create'), 'icon'=>'star'),
	array('label'=>Yii::t('DownloadModule.main', 'View Download Category'),'url'=>array('view','id'=>$model->id), 'icon'=>'eye-open'),
	array('label'=>Yii::t('DownloadModule.main', 'Manage Download Categories'),'url'=>array('admin'), 'icon'=>'th-list'),
);
?>

<h1><?php echo CHtml::encode(Yii::t('DownloadModule.main','Update Download Category {recordName}',array('{recordName}'=>$model->recordName))); ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>
