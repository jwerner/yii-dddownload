<?php
$this->breadcrumbs=array(
    Yii::t('DownloadModule.main','Downloads')=>array('/download'),
	Yii::t('DownloadModule.main','Download Categories')=>array('index'),
	Yii::t('DownloadModule.main','Create'),
);

$this->menu=array(
	array('label'=>Yii::t('DownloadModule.main','List Download Categories'),'url'=>array('index'), 'icon'=>'th-large'),
	array('label'=>Yii::t('DownloadModule.main','Manage Download Categories'),'url'=>array('admin'), 'icon'=>'th-list'),
);
?>

<h1><?php echo CHtml::encode(Yii::t('DownloadModule.main','Create a New Download Category')); ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
