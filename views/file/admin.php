<?php
$this->breadcrumbs=array(
    Yii::t('DownloadModule.main','Downloads')=>array('/download'),
    Yii::t('DownloadModule.main','Download Files')=>array('index'),
    Yii::t('DownloadModule.main','Manage'),
);

$this->menu=array(
    array('label'=>Yii::t('DownloadModule.main','Create New Download File'),'url'=>array('create'), 'icon'=>'star'),
    array('label'=>Yii::t('DownloadModule.main','List Download Files'),'url'=>array('index'), 'icon'=>'th-large'),
    array('label'=>Yii::t('DownloadModule.main','Related Links'),'itemOptions'=>array('class'=>'nav-header')),
    array('label'=>Yii::t('DownloadModule.main','Download Categories'),'url'=>array('category/admin'), 'icon'=>'th-list'),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
    $('.search-form').toggle();
    $('#DownloadFile_title').focus().select();
    return false;
});
$('.search-form form').submit(function(){
    $.fn.yiiGridView.update('download-file-grid', {
        data: $(this).serialize()
    });
    return false;
});
");
?>

<h1><?php echo CHtml::encode(Yii::t('DownloadModule.main','Manage Download Files')); ?></h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link(Yii::t('DownloadModule.main','Advanced Search'),'#',array('class'=>'search-button btn')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
    'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView',array(
    'id'=>'download-file-grid',
    'type'=>'striped condensed',
    'dataProvider'=>$model->search(),
    'filter'=>$model,
    'htmlOptions'=>array('style'=>'cursor: pointer;'),
    'selectionChanged'=>"function(id){window.location='" . Yii::app()->urlManager->createUrl('/download/file/view', array('id'=>'')) . "/' + $.fn.yiiGridView.getSelection(id);}",
    'columns'=>array(
        array(
            'class'=>'bootstrap.widgets.TbButtonColumn',
            'template'=>'{view}{update}',
        ),
        array(
            'name'=>'categoryId',
            'value'=>'CHtml::link($data->category->recordName,array("category/view", "id"=>$data->categoryId))',
            'type'=>'raw',
            'filter'=>CHtml::listData(DownloadCategory::model()->findAll(array('order'=>'title')), 'id', 'title'),
        ),      
        'title',
        'subTitle',
        'fileName',
        array(
            'name'=>'mimeType',
        ),
        array(
            'name'=>'version',
            'htmlOptions'=>array('style'=>'text-align:center'),
        ),
        array(
            'name'=>'downloadCounter',
            'htmlOptions'=>array('style'=>'text-align:center'),
        ),
        array(
            'class'=>'bootstrap.widgets.TbButtonColumn',
            'template'=>'{delete}',
        ),
    ),
)); ?>
