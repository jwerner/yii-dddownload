<?php
$this->breadcrumbs=array(
    Yii::t('DownloadModule.main','Downloads')=>array('/download/category/index'),
	Yii::t('DownloadModule.main','Download Files'),
);

$this->menu=array(
	array('label'=>Yii::t('DownloadModule.main','Create New Download File'),'url'=>array('create'), 'icon'=>'star'),
	array('label'=>Yii::t('DownloadModule.main','Manage'),'url'=>array('admin'), 'icon'=>'th-list'),
    array('label'=>Yii::t('DownloadModule.main','Related Links'),'itemOptions'=>array('class'=>'nav-header')),
    array('label'=>Yii::t('DownloadModule.main','Download Categories'),'url'=>array('category/admin'), 'icon'=>'th-list'),
);
?>

<h1><?php echo CHtml::encode(Yii::t('DownloadModule.main','Download Files')); ?></h1>

<?php if(isset($_GET['DownloadFile']['categoryId'])) : $category = DownloadCategory::model()->findByPk($_GET['DownloadFile']['categoryId']); ?>
    <?php if(!is_null($category)) : ?>
    <?php $this->renderPartial('/category/_view', array('data'=>$category)); ?>
    <?php endif; ?>
<?php endif; ?>


<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
    'itemView'=>'_view',
    'template'=>'{sorter}{pager}{summary}{items}{sorter}{pager}',
    'sortableAttributes'=>array(
        'categoryId',
        'title',
        'subTitle',
        // TODO: Uncomment the following attributes to show them as sort links in list view
    // 'pathToFile',
        // 'description',
        // 'version',
    ),
)); ?>
