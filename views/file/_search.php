<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
    'action'=>Yii::app()->createUrl($this->route),
    'method'=>'get',
    'type'=>'horizontal',
    'htmlOptions'=>array('class'=>'well'),
)); ?>

    <?php echo $form->dropDownListRow($model,'categoryId',CHtml::listData(DownloadCategory::model()->findAll(array('order'=>'title')),'id','recordName'), array('size'=>4, 'class'=>'span5', 'multiple'=>'multiple')); ?>

    <?php echo $form->textFieldRow($model,'title',array('class'=>'span5','maxlength'=>255)); ?>

    <?php echo $form->textFieldRow($model,'subTitle',array('class'=>'span5','maxlength'=>255)); ?>

    <?php echo $form->textFieldRow($model,'pathToFile',array('class'=>'span5')); ?>

    <?php echo $form->textFieldRow($model,'description',array('class'=>'span5')); ?>

    <?php echo $form->textFieldRow($model,'version',array('class'=>'span5','maxlength'=>20)); ?>

    <?php echo $form->textFieldRow($model,'downloadCounter',array('class'=>'span5')); ?>

    <div class="form-actions">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'type'=>'primary',
            'label'=>Yii::t('DownloadModule.main','Search'),
        )); ?>
    </div>

<?php $this->endWidget(); ?>
