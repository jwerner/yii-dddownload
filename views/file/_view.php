<div class="navbar-inner" style="padding:15px">

    <div class="row-fluid">

    <div class="span3">
        <p>
        <?php echo CHtml::link(
            CHtml::image(
                $this->createUrl('preview', array('image'=>str_replace(DIRECTORY_SEPARATOR,'|',$data->pathToFile.DIRECTORY_SEPARATOR.md5($data->fileName)), 'x'=>100)),
                $data->fileName, 
                array('width'=>100, 'style'=>'border:1px solid #aaa;padding:3px;')
            ),
            $this->createUrl('download',array('id'=>$data->id)),
            array('title'=>Yii::t('DownloadModule.main','Download {fileName}', array('{fileName}'=>$data->fileName)))
        ); ?>
        </p>

        <p>
        <a class="btn btn-mini btn-primary" href="<?php echo $this->createUrl('download',array('id'=>$data->id)); ?>"><i class="icon-download-alt icon-white"></i>&nbsp;<?php echo Yii::t('DownloadModule.main','Download File'); ?></a>
        </p>

    </div>

    <div class="span3">

        <b><?php echo CHtml::encode($data->getAttributeLabel('categoryId')); ?>:</b><br />
        <?php echo CHtml::link($data->category->recordName,array('file/index', 'DownloadFile[categoryId]'=>$data->categoryId)); ?>
        <br />

        <b><?php echo CHtml::encode($data->getAttributeLabel('title')); ?>:</b><br />
        <?php echo CHtml::encode($data->title); ?>
        <br />

        <b><?php echo CHtml::encode($data->getAttributeLabel('fileName')); ?>:</b><br />
        <?php echo CHtml::encode($data->fileName); ?>

    </div>
    
    <div class="span3">

        <b><?php echo CHtml::encode($data->getAttributeLabel('subTitle')); ?>:</b><br />
        <?php echo CHtml::encode($data->subTitle); ?>
        <br />

        <b><?php echo CHtml::encode($data->getAttributeLabel('description')); ?>:</b><br />
        <?php echo CHtml::encode($data->description); ?>
        <br />

        <b><?php echo CHtml::encode($data->getAttributeLabel('version')); ?>:</b><br />
        <?php echo CHtml::encode($data->version); ?>
        <br />

    </div>

    </div><!-- end div.row-fluid -->

</div>
