<?php
$this->breadcrumbs=array(
    Yii::t('DownloadModule.main','Downloads')=>array('/download'),
	Yii::t('DownloadModule.main','Download Files')=>array('index'),
	$model->title=>array('view','id'=>$model->id),
	Yii::t('DownloadModule.main','Update'),
);

$this->menu=array(
	array('label'=>Yii::t('DownloadModule.main', 'Create New Download File'),'url'=>array('create'), 'icon'=>'star'),
	array('label'=>Yii::t('DownloadModule.main', 'View Download File'),'url'=>array('view','id'=>$model->id), 'icon'=>'eye-open'),
	array('label'=>Yii::t('DownloadModule.main', 'List Download Files'),'url'=>array('index'), 'icon'=>'th-large'),
	array('label'=>Yii::t('DownloadModule.main', 'Manage Download Files'),'url'=>array('admin'), 'icon'=>'th-list'),
);
?>

<h1><?php echo CHtml::encode(Yii::t('DownloadModule.main','Update Download File {recordName}',array('{recordName}'=>$model->recordName))); ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>
