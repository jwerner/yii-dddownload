<div class="form">

<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
    'id'=>'download-file-form',
    'enableAjaxValidation'=>false,
    'type'=>'horizontal',
    'htmlOptions'=>array('class'=>'well', 'enctype' => 'multipart/form-data'),
    'focus'=>array($model,($model->isNewRecord and isset($_GET['DownloadFile']['categoryId'])) ? 'title' : 'categoryId'),
)); ?>

    <p class="help-block">Fields with <span class="required">*</span> are required.</p>

    <?php echo $form->errorSummary($model); ?>

    <?php echo $form->dropDownListRow($model,'categoryId',CHtml::listData(DownloadCategory::model()->findAll(array('order'=>'title')),'id','recordName'), array('empty'=>'(Select)', 'class'=>'span5')); ?>

    <?php echo $form->textFieldRow($model,'title',array('class'=>'span5','maxlength'=>255)); ?>

    <?php echo $form->textFieldRow($model,'subTitle',array('class'=>'span5','maxlength'=>255)); ?>

    <?php echo $form->fileFieldRow($model,'pathToFile', array('hint'=>Yii::t('DownloadModule.main','Allowed file types: ').$this->module->allowedFileTypes)); ?>

    <?php echo $form->textAreaRow($model,'description',array('rows'=>3, 'class'=>'span5')); ?>

    <?php echo $form->textFieldRow($model,'version',array('class'=>'span5','maxlength'=>20)); ?>

    <div class="form-actions">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'type'=>'primary',
            'label'=>$model->isNewRecord ? Yii::t('DownloadModule.main','Create') : Yii::t('DownloadModule.main','Save'),
            'icon'=>'ok',
        )); ?>
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'reset', 
            'icon'=>'repeat', 
            'label'=>Yii::t('DownloadModule.main','Reset'))); ?>
    </div>

<?php $this->endWidget(); ?>


</div><!-- form -->
