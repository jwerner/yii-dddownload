<?php
$this->breadcrumbs=array(
    Yii::t('DownloadModule.main','Downloads')=>array('/download'),
	Yii::t('DownloadModule.main','Download Files')=>array('index'),
	$model->title,
);

$this->menu=array(
	array('label'=>Yii::t('DownloadModule.main','Update Download File'),'url'=>array('update','id'=>$model->id), 'icon'=>'pencil'),
	array('label'=>Yii::t('DownloadModule.main','User View'),'url'=>array('file/index','DownloadFile[categoryId]'=>$model->categoryId), 'icon'=>'zoom-in'),
	array('label'=>Yii::t('DownloadModule.main','Delete Download File'),'url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?'), 'icon'=>'remove'),
	array('label'=>Yii::t('DownloadModule.main','Create New Download File'),'url'=>array('create'), 'icon'=>'star'),
	array('label'=>Yii::t('DownloadModule.main','List Download Files'),'url'=>array('index'), 'icon'=>'th-large'),
	array('label'=>Yii::t('DownloadModule.main','Manage Download Files'),'url'=>array('admin'), 'icon'=>'th-list'),
    array('label'=>Yii::t('DownloadModule.main','Related Links'),'itemOptions'=>array('class'=>'nav-header')),
    array('label'=>Yii::t('DownloadModule.main','Download Categories'),'url'=>array('category/admin'), 'icon'=>'th-list'),
);
?>

<h1><?php echo CHtml::encode(Yii::t('DownloadModule.main','View Download File {recordName}',array('{recordName}'=>$model->recordName))); ?></h1>

<?php
$stats = $model->stats;
$this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'id',
        array(
            'name'=>'categoryId',
            'value'=>CHtml::link($model->category->recordName,array('category/view', 'id'=>$model->categoryId)),
            'type'=>'raw',
        ),
		'title',
		'subTitle',
		'description',
        array(
            'name'=>'pathToFile',
            'value'=>$model->getRelativePath(),
        ),
        'fileName',
        'version',
        array(
            'label'=>'Preview',
            'type'=>'raw',
            'value'=>CHtml::image($this->createUrl('preview', array('image'=>str_replace(DIRECTORY_SEPARATOR,'|',$model->pathToFile.DIRECTORY_SEPARATOR.md5($model->fileName)), 'x'=>100), array('width'=>100))),
        ),
        array(
            'label'=>'Download',
            'type'=>'raw',
            'value'=>'<a class="btn btn-primary" '
                .'href="'.$this->createUrl('download',array('id'=>$model->id)).'">'
                .'<i class="icon-download-alt icon-white"></i> '
                .Yii::t('DownloadModule.main','Download {fileName}', array('{fileName}'=>$model->fileName))
                .'</a>',
        ),
        array(
            'label'=>'Size',
            'value'=>$model->formatBytes($stats['size']),
        ),
        array(
            'label'=>'Last Accessed',
            'value'=>Yii::app()->dateFormatter->formatDateTime($stats['atime'],'medium','short'),
        ),
        array(
            'label'=>'Last Inode Change Time',
            'value'=>Yii::app()->dateFormatter->formatDateTime($stats['ctime'],'medium','short'),
        ),
        array(
            'label'=>'Last Modification Time',
            'value'=>Yii::app()->dateFormatter->formatDateTime($stats['ctime'],'medium','short'),
        ),
        array(
            'name'=>'downloadCounter',
            'type'=>'raw',
            'value'=>$model->downloadCounter>0 ? CHtml::link(Yii::t('DownloadModule.main','{downloadCounter} (Reset)', array('{downloadCounter}'=>$model->downloadCounter)), array('resetDownloadCounter', 'id'=>$model->id)) : '0',
        ),
        array(
            'label'=>'Last Download Timestamp',
            'value'=>Yii::app()->dateFormatter->formatDateTime($model->lastDownloadedTs,'medium','short'),
        ),
	),
));
?>

<?php $this->widget('ext.diggindata.ddtimestamp.widgets.DDTimestamp', array(
    'model'=>$model, 
    'title'=>Yii::t('site','Change Log'),
    'detailViewWidget'=>'bootstrap.widgets.TbDetailView'
)); ?>
