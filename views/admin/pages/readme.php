<?php
$this->pageTitle=Yii::t('site',"To Do's").' :: '.Setting::getSetting('site.siteName');
$this->breadcrumbs=array(
	Yii::t('DownloadModule.main','Download')=>array('/download'),
	Yii::t('DownloadModule.main','README'),
);
?>

<?php
    $this->beginWidget('CMarkdown', array('purifyOutput'=>true));
    $readme = file_get_contents(dirname(__FILE__).'/../../../README.md');
    echo $readme;
    $this->endWidget();
?>
