<?php
/* @var $this DefaultController */

$this->breadcrumbs=array(
	Yii::t('DownloadModule.main','Download'),
);
?>
<h1><?php echo CHtml::encode('Downloads'); ?></h1>

<div class="row-fluid">
    <div class="span4">
        <h2><?php echo CHtml::encode(Yii::t('DownloadModule.main','Categories')); ?></h2>
        <ul>
            <li><?php echo CHtml::link('<i class="icon-star"></i> '.Yii::t('DownloadModule.main','Create New Category'), array('category/create')); ?></li>
            <li><?php echo CHtml::link('<i class="icon-th-list"></i> '.Yii::t('DownloadModule.main','Manage Categories'), array('category/admin')); ?></li>
        </ul>
    </div>
    <div class="span4">
        <h2><?php echo CHtml::encode(Yii::t('DownloadModule.main','Files')); ?></h2>
        <ul>
            <li><?php echo CHtml::link('<i class="icon-star"></i> '.Yii::t('DownloadModule.main','Create New File'), array('file/create')); ?></li>
            <li><?php echo CHtml::link('<i class="icon-th-list"></i> '.Yii::t('DownloadModule.main','Manage Files'), array('file/admin')); ?></li>
        </ul>
    </div>
</div>

<p><?php echo CHtml::link('View README', array('page', 'view'=>'README')); ?></p>
